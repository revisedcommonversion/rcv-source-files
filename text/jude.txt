////
  The Holy Bible: Revised Common Version
  Copyright (c) 2025 William Masopust
  http://www.revisedcommonversion.com
  The source code of the RCV text is available at http://source.rcv.xyz.
  
  This project and the accompanying materials are made available under the 
  terms of the Eclipse Public License 2.0 which is available at 
  https://www.eclipse.org/legal/epl-2.0/.
  
  SPDX-License-Identifier: EPL-2.0
////
[#jude]
== The Letter from Jude

[#jde001%breakable]
=== Chapter 1

[.outline.text-center]
_Jude exhorts them to be constant in the profession of the faith, 1-3. False teachers have crept in to seduce them, for whose evil doctrine and manners horrible punishment is prepared, 4-19; whereas the godly, by the assistance of the Holy Spirit and prayers to God, may persevere, grow in grace, keep themselves, and recover others out of the snares of those deceivers, 20-25._

1 Jude, the servant of Jesus Christ and brother of James, to those who are sanctified by God the Father, preserved in Jesus Christ, and called: 2 May mercy, peace, and love be multiplied to you.

3 Beloved, when I gave all diligence to write to you concerning the common salvation, it was needful for me to write to you and exhort you that you should earnestly contend for the faith, which was once delivered to the saints. 4 For some men have crept in unexpectedly, who were before of old ordained to this condemnation, ungodly men, turning the grace of our God into lasciviousness and denying the only Lord God and our Lord Jesus Christ.

5 I will therefore put you in remembrance, though you once knew this, that the Lord, having saved the people out of the land of Egypt, afterward destroyed those who did not believe. 6 And the angels who did not keep their first state, but left their own habitation, he has reserved in everlasting chains under darkness to the judgment of the great day, 7 Even as Sodom and Gomorrah, and the cities around them in like manner, giving themselves over to impurity and going after strange flesh, are set forth for an example, suffering the vengeance of eternal fire.

8 Likewise also, these filthy dreamers defile the flesh, despise dominion, and speak evil of glorious [ones]. 9 Yet Michael the archangel, when contending with the devil and disputing about the body of Moses, did not dare bring against him a railing accusation, but said, "May the Lord rebuke you." 10 But these speak evil of those things which they do not know. But what they know naturally as brute beasts, in those things they corrupt themselves. 11 Woe to them! For they have gone in the way of Cain, have run greedily after the error of Balaam for reward, and have perished in the gainsaying of Korah. 12 These are spots in your feasts of charity when they feast with you, feeding themselves without fear. They are clouds without water carried along by winds. They are withered autumn trees, without fruit, twice dead, plucked out by the roots. 13 They are raging waves of the sea foaming out their own shame. They are wandering stars to whom the blackness of darkness is reserved forever.

14 And Enoch also, the seventh from Adam, prophesied of these, saying, "Behold, the Lord comes with tens of thousands of his saints, 15 To execute judgment upon all and to convict all who are ungodly among them of all their ungodly deeds which they have impiously committed and of all their hard speeches which ungodly sinners have spoken against him." 16 These are murmurers and complainers, walking after their own lusts, and their mouth speaks great swelling words, having men's persons in admiration for the sake of profit.

17 But, beloved, remember the words which were spoken before by the apostles of our Lord Jesus Christ, 18 That they told you, "There would be mockers in the last time who would walk after their own ungodly lusts." 19 These are those who separate themselves. They are sensual, not having the Spirit. 20 But you, beloved, building up yourselves on your most holy faith, praying by the Holy Spirit, 21 Keep yourselves in the love of God, looking for the mercy of our Lord Jesus Christ to eternal life. 22 And on some have compassion, making a difference, 23 And others, save with fear, pulling them out of the fire, hating even the garment spotted by the flesh.

24 Now to him who is able to keep you from falling and to present you faultless before the presence of his glory with exceeding joy, 25 To God the only wise, our Savior, be glory and majesty, dominion and power, both now and forever. Amen.
