////
  The Holy Bible: Revised Common Version
  Copyright (c) 2025 William Masopust
  http://www.revisedcommonversion.com
  The source code of the RCV text is available at http://source.rcv.xyz.
  
  This project and the accompanying materials are made available under the 
  terms of the Eclipse Public License 2.0 which is available at 
  https://www.eclipse.org/legal/epl-2.0/.
  
  SPDX-License-Identifier: EPL-2.0
////
[#malachi]
== The Word of the Lord to Malachi

[#mal001%breakable]
=== Chapter 1

[.outline.text-center]
_Malachi complains of Israel's unkindness, 1-5; of their irreligiousness and profaneness, 6-14._

1 [_The burden of the word of the LORD to Israel by Malachi._]

[[mal001-002]]2 "I have loved you," says the LORD. Yet you say, "In what have you loved us?" "Was Esau not Jacob's brother?" says the LORD. "Yet I loved Jacob, [[mal001-003]]3 And I hated Esau, and laid his mountains and his heritage waste for the jackals of the wilderness." 4 Whereas Edom says, "We are impoverished, but we will return and build the desolate places," thus says the LORD of hosts: "They shall build, but I will throw down, and they shall call them, 'The Border of Wickedness,' and, 'The People Against Whom the LORD Has Indignation Forever.'" 5 And your eyes shall see, and you shall say, "The LORD will be magnified from the border of Israel."

6 "'A son honors his father and a servant his master. If then I am a father, where is my honor? And if I am a master, where is my fear?' says the LORD of hosts to you, O priests, who despise my name." And you say, "In what have we despised your name?" 7 "You offer polluted bread upon my altar." But you say, "In what have we polluted you?" In that you say, "The table of the LORD is contemptible." 8 "And if you offer the blind for sacrifice, 'It is not evil!' And if you offer the lame and sick, 'It is not evil!' Offer it now to your governor. Will he be pleased with you, or accept your person?" says the LORD of hosts. 9 "And now, I pray you, implore God that he will be gracious to us. This has been by your means. Will he regard your person?" says the LORD of hosts. 10 "Oh that there were one of you who would shut the doors so that you would not kindle fire on my altar in vain! I have no pleasure in you," says the LORD of hosts, "neither will I accept an offering at your hand. 11 For from the rising of the sun even to the setting of the same, my name shall be great among the Gentiles, and in every place incense shall be offered to my name, even a pure offering, for my name shall be great among the heathen," says the LORD of hosts. 12 "But you have profaned it, in that you say, 'The table of the LORD is polluted, and its fruit, even his provision, is contemptible.' 13 You also said, 'Behold, what a weariness it is!' And you have snuffed at it," says the LORD of hosts, "and you brought that which was torn, the lame, and the sick. Thus you brought an offering. Should I accept this from your hand?" says the LORD. 14 "But cursed is the deceiver who has in his flock a male, and vows and sacrifices to the Lord a corrupt thing. For I am a great King," says the LORD of hosts, "and my name is dreadful among the heathen."

[%breakable]
References::
*[v.2]*: Quoted in xref:#rom009-013[Romans 9:13]. *[v.3]*: Quoted in xref:#rom009-013[Romans 9:13].

[#mal002%breakable]
=== Chapter 2

[.outline.text-center]
_He sharply reproves the priests for neglecting their covenant, 1-9; and the people for marrying strange wives, 10-12; and for putting away their former ones, 13-16; and for infidelity, 17._

1 "And now, O priests, this commandment is for you. 2 If you will not hear and if you will not lay it to heart to give glory to my name," says the LORD of hosts, "I will even send a curse upon you and I will curse your blessings. I have even cursed them already, because you do not lay it to heart. 3 Behold, I will rebuke your offspring and spread dung upon your faces, even the dung of your solemn feasts, and you shall be taken away with it. 4 And you shall know that I have sent this commandment to you, so that my covenant might be with Levi," says the LORD of hosts. 5 "My covenant was with him of life and peace, and I gave them to him for the fear with which he feared me and was afraid before my name. 6 The law of truth was in his mouth and iniquity was not found in his lips. He walked with me in peace and equity and turned many away from iniquity. 7 For the priest's lips should keep knowledge, and they should seek the law from his mouth, for he is the messenger of the LORD of hosts. 8 But you have departed out of the way. You have caused many to stumble at the law. You have corrupted the covenant of Levi," says the LORD of hosts. 9 "Therefore, I have also made you contemptible and base before all the people, according as you have not kept my ways, but have been partial in the law."

10 Have we not all one father? Has one God not created us? Why do we each deal treacherously against his brother by profaning the covenant of our fathers? 11 Judah has dealt treacherously, and an abomination is committed in Israel and in Jerusalem, for Judah has profaned the holiness of the LORD whom he loved and has married the daughter of a strange god. 12 The LORD will cut off the man who does this, the master and the scholar, out of the tabernacles of Jacob, and him who offers an offering to the LORD of hosts.

13 And this you have done again, covering the altar of the LORD with tears, with weeping, and with crying out, because he does not regard the offering anymore, or receive it with good will at your hand. 14 Yet you say, "Why?" Because the LORD has been witness between you and the wife of your youth, against whom you have dealt treacherously. Yet she is your companion and the wife of your covenant. 15 And did he not make them one, having in him the remnant of the Spirit? And why one? So that he might seek a godly offspring. Therefore, take heed to your spirit, and let no one deal treacherously against the wife of his youth. 16 "For I hate putting away," says the LORD, the God of Israel, "and the one who covers violence with his garment," says the LORD of hosts. "Therefore, take heed to your spirit so that you do not deal treacherously."

17 You have wearied the LORD with your words. Yet you say, "In what have we wearied him?" When you say, "Everyone who does evil is good in the sight of the LORD, and he delights in them," or, "Where is the God of judgment?"

[#mal003%breakable]
=== Chapter 3

[.outline.text-center]
_Of the messenger, majesty, and grace of Christ, 1-6. Of the rebellion, 7, sacrilege, 8-12, and infidelity of the people, 13-15. The promise of blessing to those who fear God, 16-18._

[[mal003-001]]1 "Behold, I will send my messenger, and he shall prepare the way before me. And the Lord, whom you seek, will suddenly come to his temple, even the messenger of the covenant, whom you delight in. Behold, he will come," says the LORD of hosts. 2 But who may endure the day of his coming? And who shall stand when he appears? For he is like a refiner's fire and like fuller's soap. 3 And he shall sit as a refiner and purifier of silver, and he shall purify the sons of Levi and purge them as gold and silver so that they may offer to the LORD an offering in righteousness. 4 Then the offering of Judah and Jerusalem shall be pleasant to the LORD, as in the days of old and as in former years.

5 "And I will come near to you for judgment and I will be a swift witness against the sorcerers, against the adulterers, against false swearers, and against those who oppress the hireling in his wages, the widow, and the fatherless, and who turn aside the stranger from his right and do not fear me," says the LORD of hosts. 6 "For I am the LORD and I do not change; therefore, you sons of Jacob are not consumed.

7 "Even from the days of your fathers you have gone away from my ordinances and have not kept them. Return to me, and I will return to you," says the LORD of hosts. "But you said, 'In what shall we return?'

8 "Will a man rob God? Yet you have robbed me. But you say, 'In what have we robbed you?' In tithes and offerings. 9 You are cursed with a curse, for you have robbed me—even this whole nation. 10 Bring all the tithes into the store-house, so that there may be food in my house, and prove me now with this," says the LORD of hosts, "if I will not open to you the windows of heaven and pour you out a blessing, so that there shall not be room enough to receive it. 11 And I will rebuke the devourer for your sakes, and he shall not destroy the fruits of your ground, neither shall your vine cast her fruit before the time in the field," says the LORD of hosts. 12 "And all nations shall call you blessed, for you shall be a delightful land," says the LORD of hosts.

13 "Your words have been stout against me," says the LORD. "Yet you say, 'What have we spoken so much against you?' 14 You have said, 'It is vain to serve God. And what profit is it that we have kept his ordinance and that we have walked mournfully before the LORD of hosts? 15 And now we call the proud happy. Even those who work wickedness are built up. Even those who test God are delivered.'"

16 Then those who feared the LORD spoke often to one another. And the LORD listened and heard it, and a book of remembrance was written before him for those who feared the LORD and who esteemed his name. 17 "And they shall be mine," says the LORD of hosts, "even my own peculiar treasure, in the day that I prepare. And I will spare them, as a man spares his own son who serves him." 18 Then you shall return and discern between the righteous and the wicked, between him who serves God and him who does not serve him.

[%breakable]
References::
*[v.1]*: Quoted in xref:#mth011-010[Matthew 11:10]; xref:#mrk001-002[Mark 1:2]; xref:#luk001-076[Luke 1:76], xref:#luk007-027[7:27].

[#mal004%breakable]
=== Chapter 4

[.outline.text-center]
_God's judgment on the wicked, 1; and his blessing on the good, 2, 3. He exhorts to the study of the law, 4; and tells of Elijah's coming and office, 5, 6._

1 "For behold, the day comes that shall burn as an oven, and all the proud and all who do wickedly shall be stubble. And the day that comes shall burn them up," says the LORD of hosts, "so that it shall leave them neither root nor branch. 2 But to you who fear my name, the Sun of righteousness shall arise with healing in his wings, and you shall go forth and grow up as calves of the stall. 3 And you shall tread down the wicked, for they shall be ashes under the soles of your feet in the day which I am preparing," says the LORD of hosts.

4 "Remember the law of Moses my servant, which I commanded to him in Horeb for all Israel, with the statutes and judgments.

5 "Behold, I will send you Elijah the prophet before the coming of the great and dreadful day of the LORD. [[mal004-006]]6 And he shall turn the heart of the fathers to the children, and the heart of the children to their fathers, lest I come and smite the earth with a curse."

[%breakable]
References::
*[v.6]*: Quoted in xref:#luk001-017[Luke 1:17].

[.text-center]
_[Tanach reading: go to xref:#psalms[Psalms]]_
